package com.socialgroup.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import brave.sampler.Sampler;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableZuulProxy
@EnableSwagger2
public class SocialGroupProxyApplication {

	public static void main(final String[] args) {
		SpringApplication.run(SocialGroupProxyApplication.class, args);
	}

	/**
	 * Ne pas supprimer
	 * Ce bean permet de passer à tracer à true le boolean du paramètre d'export de la librairie Sleuth
	 * @return
	 */
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
}
