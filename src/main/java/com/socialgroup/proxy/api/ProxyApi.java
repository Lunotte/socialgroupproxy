package com.socialgroup.proxy.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import com.socialgroup.proxy.api.filter.ErrorFilter;
import com.socialgroup.proxy.api.filter.PostFilter;
import com.socialgroup.proxy.api.filter.PreFilter;
import com.socialgroup.proxy.api.filter.RouteFilter;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

@Configuration
public class ProxyApi {

	@Autowired
	ZuulProperties properties;

	@Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

	@Bean
	public PreFilter preFilter() {
        return new PreFilter();
    }

	@Bean
	public RouteFilter routeFilter() {
        return new RouteFilter();
    }

	@Bean
	public PostFilter postFilter() {
        return new PostFilter();
    }

	@Bean
	@ConditionalOnProperty(name="zuul.SendErrorFilter.error.disable")
	public ErrorFilter errorFilter() {
        return new ErrorFilter();
    }

	@Primary
	@Bean
	public SwaggerResourcesProvider swaggerResourcesProvider() {
		return () -> {
			final List<SwaggerResource> resources = new ArrayList<>();
			properties.getRoutes().values().stream()
					.forEach(route -> resources.add(createResource(route.getServiceId(), route.getId(), "2.0")));
			return resources;
		};
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.any()).paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo())
				.securitySchemes(Arrays.asList(apiKey()));
	}

	 private ApiInfo apiInfo() {
		 return new ApiInfo("My REST API", "Some custom description of API.", "API TOS", "Terms of service",
					new Contact(null, null, null), "", "", Collections.emptyList());
	}

	private ApiKey apiKey() {
	    return new ApiKey("Bearer", "X-Authorization", "header");
	}

	private SwaggerResource createResource(final String name, final String location, final String version) {
		final SwaggerResource swaggerResource = new SwaggerResource();
		swaggerResource.setName(name);
		swaggerResource.setLocation("/api/" + location + "/v2/api-docs");
		swaggerResource.setSwaggerVersion(version);
		return swaggerResource;
	}

}
