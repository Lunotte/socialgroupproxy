package com.socialgroup.proxy.api.filter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class RouteFilter extends ZuulFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(RouteFilter.class);

	@Value("${zuul.routes.auth.serviceId}")
	private String authApiPath;

	@Autowired
	private RestTemplate restTemplate;

    @Override
    public String filterType() {
    	 return FilterConstants.ROUTE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.SIMPLE_HOST_ROUTING_FILTER_ORDER + 1;
    }

    @Override
    public boolean shouldFilter() {
    	/*return RequestContext.getCurrentContext().getRouteHost() != null
				&& RequestContext.getCurrentContext().sendZuulResponse();*/
    	return false;
    }

	@Override
	public Object run() throws ZuulException {

		final RequestContext ctx = RequestContext.getCurrentContext();
	    final HttpServletRequest request = ctx.getRequest();

		final HttpHeaders headers = new HttpHeaders();
		final String token = request.getHeader("X-Authorization");
		headers.set("X-Authorization", token);
		final HttpEntity<String> entity = new HttpEntity<>("body", headers);
		final ResponseEntity<Object> user = restTemplate.exchange("http://"+authApiPath+"/me", HttpMethod.GET, entity, Object.class);

		LOGGER.debug("User logged {}", user);
		request.setAttribute("userContext", "test");
		ctx.addZuulRequestHeader("userContext", "test");
		return null;
	}

}
