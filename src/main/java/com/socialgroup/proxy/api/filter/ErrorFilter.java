package com.socialgroup.proxy.api.filter;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.post.SendErrorFilter;

import com.netflix.zuul.context.RequestContext;

public class ErrorFilter extends SendErrorFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorFilter.class);

//	public static final String IS_ERROR_CODE_AVAILABLE = "error.status_code";
//	public static final String IS_EXCEPTION_AVAILABLE = "error.exception";

//	@Override
//    public String filterType() {
//        return FilterConstants.POST_TYPE;
//    }

//    @Override
//    public int filterOrder() {
//    	// Needs to run before SendErrorFilter which has filterOrder == 0
//        return -1;
//    }

//    @Override
//    public boolean shouldFilter() {
//        // only forward to errorPath if it hasn't been forwarded to already
//        return RequestContext.getCurrentContext().containsKey(IS_ERROR_CODE_AVAILABLE);
//    }

    @Override
    public Object run() {
    	 // Rewrite the run method
         try{
             final RequestContext ctx = RequestContext.getCurrentContext();
             // Direct reuse of exception handling class
             final ExceptionHolder exception = findZuulException(ctx.getThrowable());
             LOGGER.info("Exception Information: {}", exception.getThrowable());
             // Here can return different error codes for different exceptions
             final HttpServletResponse response = ctx.getResponse();
             final String responseException = ((org.springframework.web.client.HttpStatusCodeException) exception.getThrowable().getCause()).getResponseBodyAsString();
             //response.getOutputStream().write(("{\"code\":\"999999\",\"msg\":\"" + responseException + "\"}").getBytes());
             response.getOutputStream().write((responseException).getBytes());

         }catch (final Exception ex) {
             //ReflectionUtils.rethrowRuntimeException(ex);
             LOGGER.debug("Internal error");
         }
         return null;
    }
}