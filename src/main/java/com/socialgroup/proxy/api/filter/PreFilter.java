package com.socialgroup.proxy.api.filter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class PreFilter extends ZuulFilter {

	@Value("${zuul.routes.auth.serviceId}")
	private String authApiPath;

	private static final String PREFIX_TOKEN = "Bearer";

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public String filterType() {
		return FilterConstants.PRE_TYPE;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {

		final RequestContext ctx = RequestContext.getCurrentContext();
		final String requestURL = ctx.getRequest().getRequestURL().toString();
		// Here we only require to filter those URLs which contains "proxyurl" and
		// "/admin/".
		return !(requestURL.contains("login")) && !(requestURL.contains("/v2/api-docs"));
	}

	@Override
	public Object run() {

		final RequestContext ctx = RequestContext.getCurrentContext();
		final HttpServletRequest request = ctx.getRequest();

		final String token = request.getHeader("X-Authorization");
		if ((token == null || token.isEmpty() || !token.startsWith(PREFIX_TOKEN))) {
			ctx.setSendZuulResponse(false);
			ctx.setResponseBody("{\"code\":\"999500\",\"msg\":\"Illegal Access\"}");
			ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());// 401
			return null;
		}

		final HttpHeaders headers = new HttpHeaders();
		headers.set("X-Authorization", token);
		final HttpEntity<String> entity = new HttpEntity<>("body", headers);
		final ResponseEntity<Boolean> userExist = restTemplate.exchange("http://" + authApiPath + "/user-exist",
				HttpMethod.GET, entity, Boolean.class);
		if (userExist.getStatusCode().equals(HttpStatus.OK)) {
			ctx.addZuulRequestHeader("serviceAuth", authApiPath);
		}

		return null;

	}
}

class UserAuthenticated {

	private String userName;
	// private List<String> authorities;

	public UserAuthenticated() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(final String userName) {
		this.userName = userName;
	}

	/*
	 * public List<String> getAuthorities() { return authorities; }
	 */
	/*
	 * public void setAuthorities(List<String> authorities) { this.authorities =
	 * authorities; }
	 */
}
